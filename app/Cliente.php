<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    public $timestamps = false;
    protected $table = 'usuarios';
    protected $fillable=['name','apellido','direccion','email','password'];
}
