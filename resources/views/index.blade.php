@extends('partials.footer')
@extends('partials.styles')
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img style="height: 500px" src="{{'imagen/slider-2.jpg'}}" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
     <img style="height: 500px" src="{{'imagen/slider_1.jpg'}}" class="d-block w-100" alt="...">
   </div>
   <div class="carousel-item">
    <img style="height: 500px" src="{{'imagen/slider-2.jpg'}}" class="d-block w-100" alt="...">
  </div>
</div>
<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
  <span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
  <span class="carousel-control-next-icon" aria-hidden="true"></span>
  <span class="sr-only">Next</span>
</a>
<br><br><br>
</div>

<div class="container-fluid">

  <div class="row">
    <div class="col-md-8">
      <div class="card bg-dark text-white ">
       <a href=""><img src="{{'imagen/home_mujeres.png'}}" class="card-img img-responsive" alt="..."></a>
     </div>

   </div>
   <div class="col-md-4">
    <div class="card bg-dark text-white " >
     <a href=""><img src="{{'imagen/home_accesorios.png'}}" class="card-img" alt="..."></a>
   </div> 
 </div>
</div>
<br>
<div class="row">
	<div class="col-md-4">
    <div class="card bg-dark text-white " >
      <a href=""><img src="{{'imagen/home_ninas.png'}}" class="card-img" alt="..."></a>
    </div> 
  </div>

  <div class="col-md-4">
    <div class="card bg-dark text-white " >
     <a href=""> <img src="{{'imagen/home_ninos.png'}}" class="card-img" alt="..."></a>
   </div> 
 </div>

 <div class="col-md-4">
  <div class="card bg-dark text-white " >
   <a href=""> <img src="{{'imagen/home_hombres.png'}}" class="card-img" alt="..."></a>
 </div> 
</div>
</div> 
<br>
<div class="row">

<?php foreach($productos as $pr){ ?>
<div class="col-md-3">
     @foreach($pr->$tallas as $p)
<div class="card " style="width: 18rem;">
  <a href="{{Route('producto',$pr->id)}}"><img src="{{'imagen/'.$pr->imagen}}" class="card-img-top" alt="..."></a>
  <div class="card-body">
    <p class="card-text">{{$p->producto}}</p>
    <a href="#" style="text-align:left; color: orange; " class="card-link" style="">{{'$'.$pr->precio}}</a>
    <a href="#" class="card-link"><i style="color: skyblue; " class="fas fa-shopping-cart fa-3x"></i></a>
    @endforeach 
  </div>
</div>
</div>
<?php } ?>
<br>
</div>
<br>
<div class="collapse" id="collapseExample"> 
  <div class="row">
  @foreach($productos as $pr)
<div class="col-md-3">
<div class="card " style="width: 18rem;">
  <img src="{{'imagen/'.$pr->imagen}}" class="card-img-top" alt="...">
  <div class="card-body">
    <p class="card-text">{{$pr->producto}}</p>
    <a href="#" style="text-align:left; color: orange; " class="card-link" style="">{{'$'.$pr->precio}}</a>
    <a href="#" class="card-link"><i style="color: skyblue; " class="fas fa-shopping-cart fa-3x"></i></a>
  </div>
</div>
</div>
@endforeach 
</div>
</div>
<div class="text-right">
  <p>
    <a class="btn btn-link"  data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample"><i class="fas fa-chevron-circle-down"></i>
    </a>
  </p>

</div>
</div>


