@extends('partials.footer')
@extends('partials.styles')

<div class="header-bottom">
  <div class="container">
    <div class="row">
      <div class="col-sm-9">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="mainmenu pull-left">
      <ul class="nav navbar-nav collapse navbar-collapse">
        <li><a href="" class="">Inicio</a></li>
        <li><a href="" class="">Productos</a></li>
    </ul>
</div>
</div>

</div>
</div>
</div>     

<section>
  <div class="container">
   <div class="row">
    <div class="col-sm-3">
        <div class="left-sidebar">
            <h2>Categorias</h2>
            <div class="panel-group category-products" id="accordian">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordian" href="#categoria1">
                                <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                Categoria 1
                            </a>

                        </h4>
                    </div>
                    <div id="categoria1" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul>
                                <li><a href="">Marca 1</a></li>
                                <li><a href="">Marca 2</a></li>
                                
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordian" href="#categoria2">
                                <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                Categoria 2
                            </a>

                        </h4>
                    </div>
                    <div id="categoria2" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul>
                                <li><a href="">Marca 6</a></li>
                                <li><a href="">Marca 7</a></li>
                                <li><a href="">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordian" href="#categoria3">
                                <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                Categoria 3
                            </a>

                        </h4>
                    </div>
                    <div id="categoria3" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul>
                                <li><a href="">Marca 11</a></li>
                                <li><a href="">Marca 12</a></li>
                             
                        </div>
                    </div>
                </div>
             <!--   <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordian" href="#categoria4">
                                <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                Categoria 4
                            </a>

                        </h4>
                    </div>
                    <div id="categoria4" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul>
                                <li><a href="">Marca 16</a></li>
                                <li><a href="">Marca 17</a></li>
                          
                            </ul>
                        </div>
                    </div>
                </div>
                 <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordian" href="#categoria5">
                                <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                Categoria 5
                            </a>

                        </h4>
                    </div>
                    <div id="categoria5" class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul>
                                <li><a href="">Marca 21</a></li>
                                <li><a href="">Marca 22</a></li>
                                <li><a href="">Marca 23</a></li>
                              
                            </ul>
                        </div>
                    </div>
                </div> -->
            </div>

            <div class="brands_products">
                <h2>Marcas</h2>
                <div class="brands-name">
                    <ul class="nav nav-pills nav-stacked">
                        <li><a href=""> <span class="pull-right">(5)</span>Marca 1</a></li>
                        <li><a href=""> <span class="pull-right">(5)</span>Marca 2</a></li>
                        <li><a href=""> <span class="pull-right">(5)</span>Marca 3</a></li>
                     
                     
                    </ul>
                </div>
            </div>

        </div>
    </div>				
   @foreach($data as $pr)
    <div class="col-sm-9 padding-right">
     <div class="product-details">
      <div class="col-sm-5">
       <div class="view-product">
         <img src="{{asset('imagen/'.$pr->imagen)}}" alt="" />
         <h3>ZOOM</h3>
     </div>

     <div id="similar-product" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
         <div class="item active">
             <a href=""><img src="https://placehold.it/255x237" alt="" class="img-responsive" style="height:84px;width:71px"></a>
             <a href=""><img src="https://placehold.it/255x237" alt="" class="img-responsive" style="height:84px;width:71px"></a>
             <a href=""><img src="https://placehold.it/255x237" alt="" class="img-responsive" style="height:84px;width:71px"></a>
         </div>
         <div class="item ">
             <a href=""><img src="https://placehold.it/255x237" alt="" class="img-responsive" style="height:84px;width:71px"></a>
             <a href=""><img src="https://placehold.it/255x237" alt="" class="img-responsive" style="height:84px;width:71px"></a>
             <a href=""><img src="https://placehold.it/255x237" alt="" class="img-responsive" style="height:84px;width:71px"></a>
         </div>

     </div>

     <a class="left item-control" href="#similar-product" data-slide="prev">
         <i class="fa fa-angle-left"></i>
     </a>
     <a class="right item-control" href="#similar-product" data-slide="next">
         <i class="fa fa-angle-right"></i>
     </a>
 </div>

</div>
<div class="col-sm-7">
   <div class="product-information">
       <h4><span class="label label-success newarrival">Nuevo</span></h4>


       <h2>{{$pr->producto}}</h2>
       <p>Web ID: 125</p>
       <img src="images/product-details/rating.png" alt="" />
       <span>
         <span>CLP $125010</span>
         <form method="POST" action="https://shcartlv.gabrielflores.cl/compras/anadir-al-carro" accept-charset="UTF-8"><input name="_token" type="hidden" value="Mtry7sTwvEpxsW70Jila7tUokyVlByVb8WMRqf88">
             <input name="id" type="hidden" value="125">
             <label for="Cantidad:">Cantidad:</label>
             <input name="cantidad" type="text" value="1">
             <button class="btn btn-default cart" type="submit"><i class="fa fa-shopping-cart"></i> Añadir al carro</button>
         </form>
     </span>
     <p><b>Disponibilidad:</b> En stock</p>
     <p><b>Condicion:</b> Nuevo</p>
     <p><b>Marca:</b> Marca 25</p>
     <a href=""><img src="images/product-details/share.png" class="share img-responsive"  alt="" /></a>
 </div>
</div>
</div>
@endforeach
<div class="category-tab shop-details-tab">
  <div class="col-sm-12">
   <ul class="nav nav-tabs">
    <li><a href="#details" data-toggle="tab">Detalles</a></li>

</ul>
</div>
<div class="tab-content">
   <div class="tab-pane fade active in" id="details" >
    <div class="col-sm-11">
        <p style="color: black;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore, nesciunt eos totam sit saepe odit, numquam voluptatem officia beatae deserunt iure similique! Hic nihil illum sit dolorum blanditiis quaerat dolores.</p>
 </div>

</div>

</div>
</div>

</div>
</div>
</div>
</section>



</body>
</html>