<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Detalle de Producto | @yield('title')</title>
    @include('partials.styles')
    @yield('styles')
   
</head>
<body>

    @include('partials.header')
    
      
   
    @include('partials.footer')

   
</body>
</html>